# -*- coding: utf-8 -*-
################################################################
#    License, author and contributors information in:          #
#    __openerp__.py file at the root folder of this module.    #
################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import logging

class account_move(models.Model):

    _inherit = 'account.move'

    _logger = logging.getLogger(__name__)

    invoice = fields.Many2one(
        string=_('Invoice'),
        required=False,
        readonly=False,
        index=False,
        default=None,
        help=_('Invoice number'),
        comodel_name='account.invoice',
        domain=[],
        context={},
        ondelete='cascade',
        auto_join=False
    )
